package com.aceleeyy.action;

import org.springframework.stereotype.Component;

/**
 * MySQL新版本
 *
 * @author Ace Lee
 * @date 2019-08-29 9:35
 */
@Component("MYSQL_NEW")
public class MysqlQueryStrategy extends AbstractSqlQueryStrategy {

	/**
	 * MYSQL 8
	 */
	private static final String DRIVER = "com.mysql.cj.jdbc.Driver";

	@Override
	protected String getDriverName() {
		return DRIVER;
	}
}
