package com.aceleeyy.common.web.model;

import lombok.Data;

/**
 * 扩展数据源的结构
 */
@Data
public class ExtendSource{
	private String Id;
	private String dbName;
	private String dbType;
	private String connectionInfo;
}
