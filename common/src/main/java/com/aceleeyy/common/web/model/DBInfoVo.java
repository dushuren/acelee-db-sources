package com.aceleeyy.common.web.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class DBInfoVo implements Serializable {

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * jdbcUrl
     */
    private String url;

    /**
     * 数据库类型
     */
    private String type;

    /**
     * 数据库
     */
    private String schema;
}
