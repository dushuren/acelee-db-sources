package com.aceleeyy.common.error;

/**
 * 错误信息
 *
 * @author Ace Lee
 * @date 2019/3/5 16:46
 */
public enum ErrorMessage {

    E_001("001", "请求参数错误"),
    E_002("002", "token异常"),
    E_003("003", "查询为空"),
    E_004("004", "业务类型不存在"),
    E_005("005", "节点操作错误"),
    E_006("006", "节点更改对象错误"),
    E_007("007", "文件下载失败"),
    E_008("008", "加解密失败"),
    E_009("009", "连接信息不存在或失效"),
    E_999("999", "系统错误");

    private String errcode;
    private String errdesc;

    public String getErrCode() {
        return errcode;
    }


    public String getErrDesc() {
        return errdesc;
    }


    ErrorMessage(String errcode, String errdesc) {
        this.errcode = errcode;
        this.errdesc = errdesc;
    }


    public static String explain(String errcode) {
        for (ErrorMessage bussErrorCode : ErrorMessage.values()) {
            if (bussErrorCode.errcode.equals(errcode)) {
                return bussErrorCode.errdesc;
            }
        }
        return errcode;
    }
}
