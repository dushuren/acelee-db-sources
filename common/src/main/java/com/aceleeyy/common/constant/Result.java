package com.aceleeyy.common.constant;

import java.io.Serializable;

/**
 * 响应对象
 *
 * @author Ace Lee
 * @date 2019/3/5 16:35
 */
public class Result<T> implements Serializable {

    public Result(){}

    private boolean success;
    private String error;
    private T data;

    public Result(T data){
        this.success=true;
        this.data=data;
    }

    public static Result error(String error){
        return new Result<>(AceleeyyCommonInfo.BOOLEAN_NO,error,null);
    }

    public static Result sucess(){
        return new Result<>(AceleeyyCommonInfo.BOOLEAN_YES,"",null);
    }

    public Result(boolean success,String error,T data){
        this.success=success;
        this.error=error;
        this.data=data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
