package com.aceleeyy.action;

import org.springframework.stereotype.Component;

/**
 * SQLite
 *
 * @author Ace Lee
 * @date 2019-08-29 9:35
 */
@Component("SQLITE")
public class SqliteQueryStrategy extends AbstractSqlQueryStrategy {

	/**
	 * SQLite 3
	 */
	private static String DRIVER = "org.sqlite.JDBC";

	@Override
	protected String getDriverName() {
		return DRIVER;
	}
}
