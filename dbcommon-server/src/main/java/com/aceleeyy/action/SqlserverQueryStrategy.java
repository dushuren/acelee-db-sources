package com.aceleeyy.action;

import org.springframework.stereotype.Component;

/**
 * SQL server
 *
 * @author Ace Lee
 * @date 2019-08-29 9:35
 */
@Component("SQLSERVER")
public class SqlserverQueryStrategy extends AbstractSqlQueryStrategy {

	/**
	 * SQL SERVER 2008
	 */
	private static String DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

	@Override
	protected String getDriverName() {
		return DRIVER;
	}
}
