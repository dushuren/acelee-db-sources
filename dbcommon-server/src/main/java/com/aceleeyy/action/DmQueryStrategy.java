package com.aceleeyy.action;

import org.springframework.stereotype.Component;

/**
 * DM
 *
 * @author Ace Lee
 * @date 2019-08-29 9:35
 */
@Component("DM")
public class DmQueryStrategy extends AbstractSqlQueryStrategy {

	/**
	 * 达梦 7
	 */
	private static String DRIVER = "dm.jdbc.driver.DmDriver";

	@Override
	protected String getDriverName() {
		return DRIVER;
	}
}
