package com.aceleeyy.action;

import com.aceleeyy.common.web.model.ExtendSource;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface DataBaseQuery {

	/**
	 * 具体的sql查询
	 * @param extendSource
	 * @param sql
	 * @return
	 */
	Optional<List<Map<String,Object>>> query(ExtendSource extendSource, String sql);

	/**
	 * 具体的sqls查询
	 * @param extendSource
	 * @param sqls
	 * @return
	 */
	Optional<Map<String, Object>> querys(ExtendSource extendSource, Map<String, String> sqls);

	/**
	 * sql执行增删改
	 * @param extendSource
	 * @param sql
	 * @return
	 */
	Optional<Integer> executeDML(ExtendSource extendSource, String sql);

}
