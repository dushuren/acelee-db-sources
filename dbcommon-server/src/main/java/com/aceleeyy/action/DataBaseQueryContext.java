package com.aceleeyy.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class DataBaseQueryContext {

	@Autowired
	private Map<String, DataBaseQuery> contextStrategy = new ConcurrentHashMap<>();

	public DataBaseQuery build(String type) {
		return contextStrategy.get(type);
	}

}
