# acelee-db-sources

#### 介绍
springboot动态添加多种数据库类型的数据源，并操作数据库，包含MySQL、SQLserver、Oracle、sqlite、DB2、达梦数据库等

#### 软件架构

springboot 

Spring Cloud Eureka

Spring Cloud Zuul

Swagger

logback


#### 安装教程

1. 修改dbcommon-server、dbspecial-server里的application.yml文件里的数据源配置信息
2. mvn clean 
3. mvn install
4. 依次启动eureka、dbcommon-server、dbspecial-server、zuul的XXXXApliction.java的main()方法

#### 使用说明

1. 新增数据库类型

    1）在dbcommon-server的pom里增加数据库jdbc依赖，没有线上的依赖使用本地依赖，将jar拷贝到src/lib下面，可以参考达梦数据库的依赖；
    
    2）在dbcommon-server的src/main/java下的com.aceleeyy.action添加对应的数据源策略，里面是driver-class的提供；如果版本冲突
    在dbspecial-server对应的包下面添加另一版本的策略，提供driver-class，可以参考MySQL的
    
    3）在dbcommon-server的controller包下的DBController下面增加新数据库的类型，并实现元数据的获取；
    
2. 注意默认的数据源必须要有一个，本例中默认的数据源在application.yml配置；



    